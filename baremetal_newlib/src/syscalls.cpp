#include <cerrno>
#include <sys/stat.h>
#include <sys/times.h>

extern "C" void* _sbrk(int) { return reinterpret_cast<void*>(-1); }

extern "C" int _close(int) { return -1; }

extern "C" int _fstat(int, struct stat* st) {
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" int _isatty(int) { return 1; }

extern "C" int _lseek(int, int, int) { return 0; }

extern "C" [[noreturn]] void _exit(int) {
    for (;;)
        ;
}

extern "C" void _kill(int, int) {
    errno = EINVAL;
    return;
}

extern "C" int _getpid() { return -1; }

extern "C" int _write(int, char*, int) { return -1; }

extern "C" int _read(int, char*, int) { return -1; }

extern "C" int _open(const char*, int, int) { return -1; }

extern "C" int _stat(char*, struct stat* st) {
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" int _link(char*, char*) {
    errno = EMLINK;
    return -1;
}

extern "C" int _unlink(char*) {
    errno = ENOENT;
    return -1;
}

extern "C" int _wait(int*) {
    errno = ECHILD;
    return -1;
}

extern "C" int _execve(char*, char**, char**) {
    errno = ENOMEM;
    return -1;
}

extern "C" int _fork() {
    errno = EAGAIN;
    return -1;
}

extern "C" int _times(struct tms*) { return -1; }
