set(TARGET baremetal_newlib)

add_library(${TARGET} STATIC)
add_library(baremetal::newlib ALIAS ${TARGET})

target_sources(${TARGET}
    PRIVATE
    src/syscalls.cpp
    )

target_link_options(${TARGET}
    INTERFACE
    LINKER:--undefined=_close
    LINKER:--undefined=_fstat
    LINKER:--undefined=_isatty
    LINKER:--undefined=_lseek
    LINKER:--undefined=_exit
    LINKER:--undefined=_kill
    LINKER:--undefined=_getpid
    LINKER:--undefined=_write
    LINKER:--undefined=_read
    LINKER:--undefined=_open
    LINKER:--undefined=_stat
    LINKER:--undefined=_link
    LINKER:--undefined=_unlink
    LINKER:--undefined=_wait
    LINKER:--undefined=_execve
    LINKER:--undefined=_fork
    LINKER:--undefined=_times
    LINKER:--undefined=_sbrk
    )
