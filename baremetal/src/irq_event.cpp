#include "baremetal/irq_event.h"

namespace baremetal {

void irq_event::wait() {
    while (!event_) {
        asm volatile("wfi");
    }
    event_ = false;
}

void irq_event::notify() { event_ = true; }

} // namespace baremetal
