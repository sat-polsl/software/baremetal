#pragma once
#include <concepts>

namespace baremetal {

class irq_event {
public:
    void wait();
    void notify();

private:
    volatile bool event_{false};
};

// clang-format off
template<typename T>
concept irq_event_concept = requires (T event) {
    { event.wait() } -> std::same_as<void>;
    { event.notify() } -> std::same_as<void>;
};
// clang-format on

} // namespace baremetal
