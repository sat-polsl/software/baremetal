#pragma once
#include "baremetal/irq_event.h"
#include "gmock/gmock.h"
#include <vector>

namespace baremetal::mock {
struct irq_event {
    irq_event() { events.push_back(this); }

    MOCK_METHOD(void, wait, ());
    MOCK_METHOD(void, notify, ());

    inline static std::vector<irq_event*> events{};
};

static_assert(irq_event_concept<irq_event>);
} // namespace baremetal::mock
